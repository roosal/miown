/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; /**
                                                                                                                                                                                                                                                                               * Imports
                                                                                                                                                                                                                                                                               */


var _types = __webpack_require__(4);

var _types2 = _interopRequireDefault(_types);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Helper module to validate different environments
 *
 * PUBLIC API
 * ------------------------------
 *  validateBrowserEnvironment
 *  validateNodeEnvironment
 * ------------------------------
 */
exports.default = function environmentHelper() {

	/**
  * Base functionality to check any environment. Throws error if it fails, returns true if it succeeds
  *
  * @param {Object | function} environment - An object containing a name and a value property or a function returning a boolean
  * @param {Object} environment.name - The name of the environment to check
  * @param {Object} environment.value - An expression returning true or false
  * @return {boolean} - True if environment is valid, False otherwise
  */
	function validateEnvironment(environment) {
		if (typeof environment === 'function') {
			if (environment()) {
				return true;
			}
		} else if (_types2.default.isObject(environment)) {
			if (environment.value !== undefined) {
				return true;
			}
		}
	}

	/**
  * Function to validate if the current environment is the browser
  *
  * @return {boolean}
  */
	function validateBrowserEnvironment() {
		return validateEnvironment({
			name: 'browser',
			value: (typeof window === 'undefined' ? 'undefined' : _typeof(window)) !== undefined
		});
	}

	/**
  * Function to validate if the current environment is NodeJS
  *
  * @return {boolean}
  */
	function validateNodeEnvironment() {
		return validateEnvironment({
			name: 'node',
			value: !validateBrowserEnvironment() && module && module.exports
		});
	}

	/**
  * Public API of environmentHelper
  */
	return Object.freeze({
		validateBrowserEnvironment: validateBrowserEnvironment,
		validateNodeEnvironment: validateNodeEnvironment
	});
}();
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)(module)))

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _miown = __webpack_require__(2);

var _miown2 = _interopRequireDefault(_miown);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _miown2.default)();

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = createApp;

var _environment = __webpack_require__(0);

var _messageBox = __webpack_require__(5);

var _messageBox2 = _interopRequireDefault(_messageBox);

var _storage = __webpack_require__(6);

var _storage2 = _interopRequireDefault(_storage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DEBUG_APP = void 0;

/**
*
* @param {HTMLElement} root - The root Element on which to bootstrap the app. Defaults to the first element with data-app="miown" attribute.
*/
/**
 * MIOWN - Minimalistic javascript framework
 * @author Alyssa Roose
 * @version 0.1.0
 */

function createApp(root) {

	// Declare app properties
	var rootEl = void 0;
	var controllers = void 0;
	var storage = (0, _storage2.default)();
	var mainMessageBox = (0, _messageBox2.default)();
	/**
  * Initialize the app
  */
	(function initializeApp() {

		if (_environment.validateBrowserEnvironment) {
			rootEl = root || document.querySelector('[data-app="miown"]');
		}

		mainMessageBox.instant('info', 'The app is initializing');

		addController();
	})();

	function addController() {
		var fn = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : defaultController;

		fn();
	}

	function defaultController() {
		mainMessageBox.instant('info', 'Running default controller');
		setTimeout(function () {
			mainMessageBox.instant('info', 'Async message test');
		}, 3000);
		rootEL.querySelectorAll('[data-model]').forEach(function (element) {
			if (element.tagName.toLowerCase() !== 'input' || element.tagName.toLowerCase() !== 'textarea') {
				return;
			}
			element.addEventListener('change', function (event) {
				storage.put(element.dataset.model, element.value);
			});
		});
	}

	return {
		addController: addController
	};
}

/**
 * Module with generic functionality
 */
function arGenerics() {

	/**
  * Take a function with any amount of arguments and curry it
  * @param {function} fn - Function to curry
  * @returns {function} - The curried function
  */
	function curry() {
		var fn = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
		var a = arguments[1];

		// Todo - Write this function and apply it to error and implements functions
		return function () {
			for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
				args[_key] = arguments[_key];
			}

			if (args.length === 1) {
				return fn.apply(undefined, [a].concat(args));
			} else {
				return curry(fn.apply(undefined, [a].concat(args)));
			}
		};
	}
}

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.default = function () {
	/**
  * Function to check for null value
  *
  * @param {*} value - Value to check for type null
  * @returns {boolean} - True if value is null, False otherwise
  */
	function isNull(value) {
		return value == undefined && (typeof value === "undefined" ? "undefined" : _typeof(value)) === "object";
	}

	/**
  * Function to check if value is an object
  *
  * @param {*} value - Value to check for type null
  * @returns {boolean} - True if value is an object, False otherwise
  */
	function isObject(value) {
		return value != undefined && (typeof value === "undefined" ? "undefined" : _typeof(value)) === "object";
	}

	/**
  * Function to check if a value is an array.
  *
  * @param {*} value - Value to check for type array
  * @returns {boolean} - True if value is an array, False otherwise
  */
	function isArray(value) {
		return value !== undefined && Array.isArray(value);
	}

	/**
  * Function to check if a value is a function
  * 
  * @param {*} value
  * @returns {boolean} - True if value is a function, False otherwise	 */
	function isFunction(value) {
		return value !== undefined && typeof value === "function";
	}

	/**
  *
  * @param {object} interface - Object with the required API methods to implement the interface
  * @param {object} value - Object to check for correct implementation of the interface
  * @return {boolean} - True if implementation is correct, False otherwise
  */
	function hasInterface(interfac, value) {
		if (!isObject(interfac) || !isObject(value)) {
			throw TypeError('types - implements expects 2 objects as parameters');
		}

		return Object.keys(interfac).every(function (key) {
			return value[interfac.key] !== undefined;
		});
	}

	/**
  * Public API for types
  */
	return Object.freeze({
		isArray: isArray,
		isNull: isNull,
		isObject: isObject,
		hasInterface: hasInterface
	});
}();

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _environment = __webpack_require__(0);

var _environment2 = _interopRequireDefault(_environment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Todo - Check for implementation to create multiple boxes (messagelists) and the ability to add multiple printers (messengers) to each

exports.default = function () {

  /**
   * Internal message store
   */
  var messages = [];

  /**
   * Pointer for reading messages
   */
  var pointer = 0;

  /**
   * Create a new messagebox
   * @param {Object} options - An options object to customize the messagebox
   * @param {array} messages - Take a messagelist to start the initial store
   * @param {function} printer - A function which can output a message
   */
  return function createMessageBox() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$messages = _ref.messages,
        messages = _ref$messages === undefined ? [] : _ref$messages,
        _ref$printer = _ref.printer,
        printer = _ref$printer === undefined ? createQuickMessage() : _ref$printer;

    /**
     * Public API for messageBox
     */
    var api = {};

    /**
     * Add a message to the messageBox
     * 
     * @param {number} type 
     * @param {string} message 
     */
    function add(type, message) {
      if (typeof message !== "string" || typeof type !== "string") {
        throw TypeError('messagebox - add - incorrect type of arguments');
      }

      messages.push({ type: type, message: message });
    }

    /**
     * Print all messages currently in storage
     */
    function showAll() {
      if (pointer >= messages.length) {
        return;
      }

      show();
      showAll();
    }

    /**
     * Show the first message in the queue
     */
    function show() {
      printer(messages[pointer]);
      pointer += 1;
    }

    /**
     * Clear all messages which are already printed
     */
    function clear() {
      if (pointer > 0) {
        messages = messages.slice(pointer);
        pointer = 0;
      }
    }

    /**
     * Save and print a message
     * @param {string} message 
     */
    function showNow(message) {
      messages.splice(pointer, 0, [message]);
      show();
    }

    /**
     * Print a message without saving it to the messagebox
     */
    function instant(type, message) {
      printer(type, message);
    }

    /**
     * Construct api object
     */
    api.add = add;
    api.show = show;
    api.showAll = showAll;
    api.showNow = showNow;
    api.instant = instant;
    api.clear = clear;

    return api;
  };

  /**
   * Printer function - Can be used by the messageBox to log the messages
   */
  function createQuickMessage() {

    var queue = [];
    var timer = void 0;
    var index = 0;

    if (!_environment2.default.validateBrowserEnvironment()) {
      return console.log('quickMessage can only be used in a browser');
    }

    return function quickMessage() {
      var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'info';
      var message = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Test Message';


      queue.push({ type: type, message: message });

      if (timer === undefined) {
        timer = setInterval(print, 100);
      }

      function print() {

        var next = queue.shift();

        // Create html structure
        var messageEl = document.createElement('div');
        messageEl.className = "message-box-quick message-box-" + next.type;
        messageEl.innerHTML = "\n          " + next.message + "\n        ";
        messageEl.style.top = index * 40 + "px";

        index += 1;

        // Add message to DOM
        document.body.appendChild(messageEl);
        messageEl.classList.add('message-box-quick-in');
        setTimeout(function () {
          messageEl.classList.remove('message-box-quick');
          document.body.removeChild(messageEl);
          index -= 1;
        }, 2000);

        if (queue.length === 0) {
          clearInterval(timer);
          timer = undefined;
        }
      }
    };
  }
}();

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = exports = function createStore() {
  var store = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};


  /**
   * Custom bindings object
   */
  var bindings = {};

  /**
   * Public API
   */
  var api = {};

  /**
   * Set the value of the store or create it if it didn't exist
   */
  function put(name) {
    var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

    if (typeof name !== 'string') throw TypeError('Storage - put needs at least one argument which needs to be a string');

    if (store[name] === undefined) {
      store[name] = value;
      // set default bindings
    } else {
      store[name] = value;
      // update default bindings
      // TODO - Extract to binding file, make the binding optional
      updateModels(name, value);
      // update custom bindings
    }
  }

  /**
   * BINDINGS
   */
  function updateModels(name, value) {
    var boundElements = document.querySelectorAll('[data-bind=' + name + ']');
    boundElements.forEach(function (element) {
      // Handle setting value
      if (element.tagName.toLowerCase() === 'input' || element.tagName.toLowerCase() === 'textarea') {
        element.value = store[name];
      } else if (element.tagName.toLowerCase() === 'radio' || element.tagName.toLowerCase() === 'checkbox') {
        if (element.value === store[name]) {
          element.checked = true;
        } else {
          element.checked = false;
        }
      } else {
        element.textContent = store[name];
      }
    });
  }

  /**
   * 
   * @param {*} fn 
   * @param {*} storage 
   */
  function setListener(fn) {
    for (var _len = arguments.length, storage = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      storage[_key - 1] = arguments[_key];
    }

    if (fn === undefined) throw TypeError('Function bind needs at least 2 arguments where the first is a string and the second a function');

    if (fn.length !== storage.length) {
      console.log('Warning binding ' + name + '. Expected number of arguments needs to be the same as the number of storage dependencies');
    }

    // Bind the function to every storage member used in the function
    storage.forEach(function (data) {
      var dataBindings = bindings[data] || [];
      bindings[data].push([].concat(storage, [fn]));
    });
  }

  api.put = put;

  return api;
};

/***/ })
/******/ ]);