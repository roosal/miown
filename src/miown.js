/**
 * MIOWN - Minimalistic javascript framework
 * @author Alyssa Roose
 * @version 0.1.0
 */

import { validateBrowserEnvironment } from './lib/environment';
import messageBox from './lib/messageBox';
import createStore from './lib/storage';

let DEBUG_APP;

(function() {

	let miown = createApp();

	/**
	*
	* @param {HTMLElement} root - The root Element on which to bootstrap the app. Defaults to the first element with data-app="miown" attribute.
	*/
	function createApp(root) {

		// Declare app properties
		let rootEl;
		let storage;
		let messageBox = messageBox();

		/**
		 * Initialize the app
		 */
		function initializeApp() {

			if(validateBrowserEnvironment) {
				// Set the app root
				rootEl = root || document.querySelector('[data-app="miown"]');
				// Create storage module
				storage = createStore();
			}

		}

		function addController(fn = () => {}) {

		}

	}

	/**
	 * Module with generic functionality
	 */
	function arGenerics() {

		/**
		 * Take a function with any amount of arguments and curry it
		 * @param {function} fn - Function to curry
		 * @returns {function} - The curried function
		 */
		function curry(fn = () => {}, a) {
			// Todo - Write this function and apply it to error and implements functions
			return function(...args) {
				if(args.length === 1) {
					return fn(a, ...args);
				} else {
					return curry(fn(a, ...args));
				}
			}
		}

	}

}())
