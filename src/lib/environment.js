/**
 * Imports
 */
import types from './types';

/**
 * Helper module to validate different environments
 *
 * PUBLIC API
 * ------------------------------
 *  validateBrowserEnvironment
 *  validateNodeEnvironment
 * ------------------------------
 */
export default (function environmentHelper() {

	/**
	 * Base functionality to check any environment. Throws error if it fails, returns true if it succeeds
	 *
	 * @param {Object | function} environment - An object containing a name and a value property or a function returning a boolean
	 * @param {Object} environment.name - The name of the environment to check
	 * @param {Object} environment.value - An expression returning true or false
	 * @return {boolean} - True if environment is valid, False otherwise
	 */
	function validateEnvironment(environment) {
		if(typeof environment === 'function') {
			if(environment()) {
				return true;
			}
		} else if(types.isObject(environment)) {
			if(environment.value !== undefined) {
				return true;
			}
		}
	}

	/**
	 * Function to validate if the current environment is the browser
	 *
	 * @return {boolean}
	 */
	function validateBrowserEnvironment() {
		return validateEnvironment({
			name: 'browser',
			value: typeof window !== undefined
		});
	}

	/**
	 * Function to validate if the current environment is NodeJS
	 *
	 * @return {boolean}
	 */
	function validateNodeEnvironment() {
		return validateEnvironment({
			name: 'node',
			value: !validateBrowserEnvironment() && module && module.exports
		});
	}

	/**
	 * Public API of environmentHelper
	 */
	return Object.freeze({
		validateBrowserEnvironment,
		validateNodeEnvironment
	});

}());
