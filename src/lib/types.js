export default (function() {
	/**
	 * Function to check for null value
	 *
	 * @param {*} value - Value to check for type null
	 * @returns {boolean} - True if value is null, False otherwise
	 */
	function isNull(value) {
		return value == undefined && typeof value === "object";
	}

	/**
	 * Function to check if value is an object
	 *
	 * @param {*} value - Value to check for type null
	 * @returns {boolean} - True if value is an object, False otherwise
	 */
	function isObject(value) {
		return value != undefined && typeof value === "object";
	}

	/**
	 * Function to check if a value is an array.
	 *
	 * @param {*} value - Value to check for type array
	 * @returns {boolean} - True if value is an array, False otherwise
	 */
	function isArray(value) {
		return value !== undefined && Array.isArray(value);
	}

	/**
	 * Function to check if a value is a function
	 *
	 * @param {*} value
	 * @returns {boolean} - True if value is a function, False otherwise	 */
	function isFunction(value) {
		return value !== undefined && typeof value === "function";
	}

	/**
	 *
	 * @param {object} anInterface - Object with the required API methods to implement the interface
	 * @param {object} value - Object to check for correct implementation of the interface
	 * @return {boolean} - True if implementation is correct, False otherwise
	 */
	function hasInterface(anInterface, value) {
		if(!isObject(anInterface) || !isObject(value)) {
			throw TypeError('types - implements expects 2 objects as parameters');
		}

		return Object.keys(anInterface).every(key => value[anInterface.key] !== undefined);
	}

	/**
	 * Public API for types
	 */
	return Object.freeze({
		isArray,
		isNull,
		isObject,
		hasInterface
	});

}());
