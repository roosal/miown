import environment from './environment';

// Todo - Check for implementation to create multiple boxes (messagelists) and the ability to add multiple printers (messengers) to each

export default (function() {

  /**
   * Internal message store
   */
  let messages = [];

  /**
   * Pointer for reading messages
   */
  let pointer = 0;
  
  /**
   * Create a new messagebox
   * @param {Object} options - An options object to customize the messagebox
   * @param {array} messages - Take a messagelist to start the initial store
   * @param {function} printer - A function which can output a message
   */
  return function createMessageBox({ messages = [], printer = createQuickMessage() } = {}) {
  
    /**
     * Public API for messageBox
     */
    let api = {};

    /**
     * Add a message to the messageBox
     * 
     * @param {number} type 
     * @param {string} message 
     */
    function add(type, message) {
      if(typeof message !== "string" || typeof type !== "string") {
        throw TypeError('messagebox - add - incorrect type of arguments');
      }

      messages.push({ type, message });
    }

    /**
     * Print all messages currently in storage
     */
    function showAll() {
      if(pointer >= messages.length) {
        return;
      }
      
      show();
      showAll();
    }

    /**
     * Show the first message in the queue
     */
    function show() {
      printer(messages[pointer]);
      pointer += 1;
    }

    /**
     * Clear all messages which are already printed
     */
    function clear() {
      if(pointer > 0) {
        messages = messages.slice(pointer);
        pointer = 0;
      }
    }

    /**
     * Save and print a message
     * @param {string} message 
     */
    function showNow(message) {
      messages.splice(pointer, 0, [message]);
      show();
    }

    /**
     * Print a message without saving it to the messagebox
     */
    function instant(type, message) {
      printer(type, message);
    }

    /**
     * Construct api object
     */
    api.add = add;
    api.show = show;
    api.showAll = showAll;
    api.showNow = showNow;
    api.instant = instant;
    api.clear = clear;
  
    return api;
  
  }

  /**
   * Printer function - Can be used by the messageBox to log the messages
   */
  function createQuickMessage() {

    let queue = [];
    let timer;
    let index = 0;

    if(!environment.validateBrowserEnvironment()) {
      return console.log('quickMessage can only be used in a browser');
    }

    return function quickMessage(type = 'info', message = 'Test Message') {
      
      queue.push({ type, message });
      
      if(timer === undefined) {
        timer = setInterval(print, 100);
      }

      function print() {

        let next = queue.shift();

        // Create html structure
        let messageEl = document.createElement('div');
        messageEl.className = `message-box-quick message-box-${next.type}`;
        messageEl.innerHTML = `
          ${next.message}
        `;
        messageEl.style.top = `${index  * 40}px`;

        index += 1;
    
        // Add message to DOM
        document.body.appendChild(messageEl);
        messageEl.classList.add('message-box-quick-in');
        setTimeout(function() {
          messageEl.classList.remove('message-box-quick');
          document.body.removeChild(messageEl);
          index -= 1;
        }, 2000);

        if(queue.length === 0) {
          clearInterval(timer);
          timer = undefined;
        }
      }
    }
  }

}());