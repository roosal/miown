module.exports = exports = function createStore(store = {}) {

  /**
   * Custom bindings object
   */
  let bindings = {};

  /**
   * Public API
   */
  let api = {};

  /**
   * Set the value of the store or create it if it didn't exist
   */
  function put(name, value = undefined) {
    if(typeof name !== 'string') throw TypeError(`Storage - put needs at least one argument which needs to be a string`);

    if(store[name] === undefined) {
      store[name] = value;
      // set default bindings
    } else {
      store[name] = value;
      // update default bindings
      // TODO - Extract to binding file, make the binding optional
      let boundElements = document.querySelectorAll(`[data-bind=${name}]`);
      boundElements.forEach(element => {
        // Handle setting value
        if(element.tagName.toLowerCase() === 'input' || element.tagName.toLowerCase() === 'textarea') {
					element.value = store[name];
				// Handle setting checked
        } else if(element.tagName.toLowerCase() === 'radio' || element.tagName.toLowerCase() === 'checkbox') {
          if(element.value === store[name]) {
            element.checked = true;
          } else {
            element.checked = false;
					}
				// Handle setting text
        } else {
          element.textContent = store[name];
        }
			});

      updateModels(name, value);
      // update custom bindings

    }
  }

  /**
   * BINDINGS
   */
  function updateModels(name, value) {
    let boundElements = document.querySelectorAll(`[data-bind=${name}]`);
    boundElements.forEach(element => {
      // Handle setting value
      if(element.tagName.toLowerCase() === 'input' || element.tagName.toLowerCase() === 'textarea') {
        element.value = store[name];
      } else if(element.tagName.toLowerCase() === 'radio' || element.tagName.toLowerCase() === 'checkbox') {
        if(element.value === store[name]) {
          element.checked = true;
        } else {
          element.checked = false;
        }
      } else {
        element.textContent = store[name];
      }
    });
  }

  /**
   *
   * @param {*} fn
   * @param {*} storage
   */
  function setListener(fn, ...storage) {
    if(fn === undefined) throw TypeError(`Function bind needs at least 2 arguments where the first is a string and the second a function`);

    if(fn.length !== storage.length) {
      console.log(`Warning binding ${name}. Expected number of arguments needs to be the same as the number of storage dependencies`);
    }

    // Bind the function to every storage member used in the function
    storage.forEach(data => {
      let dataBindings = bindings[data] || [];
      bindings[data].push([...storage, fn]);
    });
	}

	api.put = put;
	api.setListener = setListener;

  api.put = put;

  return api;
}
