export default (function() {

  function childOf(id, element) {
    if(element.nodeName.toLowerCase() === 'body') {
      return false;
    }

    if(element.id === id) {
      return true;
    } else {
      return childOf(id, element.parentElement);
    }
  }

  return {
    childOf
  }

}());