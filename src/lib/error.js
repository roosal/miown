/**
 * Function to create a custom error
 *
 * @param {string} name - The custom error name identifying the type of error
 * @param {string} message - The message containing more details about the cause of error
 */
export default function customError(name, message) {
	const base = Error(message);
	const { message, stack, fileName, lineNumber, columnNumber } = base;

	return {
		name,
		message,
		stack,
		fileName,
		lineNumber,
		columnNumber
	}
}
