const path = require('path');

module.exports = exports = function(env) {
	return {
		entry: './src/app.js',
		output: {
			filename: 'bundle.js',
			path: path.resolve(__dirname, './dist')
		},
		resolve: {
			extensions: [
				'.js', '.json', '.css'
			]
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					exclude: /node_modules/,
					loader: 'babel-loader'
				}
			]
		}
	}
}
