const path = require('path');
const express = require('express');

let port = 8080;
let app = express();

app.use(express.static(path.resolve(__dirname, 'public')));

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});